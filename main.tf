provider "aws" {
 region = "ap-south-1"
}

variable "myhosts" {
  type = map
  default = {
   worker1 = "t2.micro"
  }
}

variable "amiid" {
  default = "testdevops"
}

module "server" {
 pemfile = var.amiid
 source = "./modules/instances"

}

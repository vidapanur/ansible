provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "example" {
  ami           = var.amiid
  instance_type = var.type
}


variable "amiid" {
  default = "ami-0c1a7f89451184c8b"
}

variable "type" {
  default = "t2.micro"
}


output "id" {
  value = aws_instance.example.id
}

